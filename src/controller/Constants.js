import { Dimensions } from "react-native"

const Constants = {
    screens: {
        home: 'Home',
        detail: 'Detail'
    },
    dimensions: {
        window: Dimensions.get('window'),
        screen: Dimensions.get('screen')
    }
}
export default Constants 