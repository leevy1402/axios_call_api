import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Constants from './Constants'
import Home from '../home/Home'
import ListItemDetail from '../listItemDetail/ListItemDetail'

const Stack = createNativeStackNavigator()

export default function RootNaviation() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName={Constants.screens.home}>
                <Stack.Screen name={Constants.screens.home} component={Home} />
                <Stack.Screen name={Constants.screens.detail} component={ListItemDetail} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}
