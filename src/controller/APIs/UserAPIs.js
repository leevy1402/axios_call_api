import axios from "axios"

export default class UserApi {
    static endpoint = {
        user: 'https://reqres.in/api/users',
        detail: 'https://reqres.in/api/users/'
    }

    static async getUser() {
        try {
            let response = await axios.get(this.endpoint.user)
            return Promise.resolve(response.data?.data)
        } catch (error) {
            console.log(error)
            return Promise.reject(error)
        }
    }

    static async getDetail(id) {
        try {
            let response = await axios.get(this.endpoint.detail + id)
            return Promise.resolve(response.data?.data)
        } catch (error) {
            console.log(error)
            return Promise.reject(error)
        }
    }
}