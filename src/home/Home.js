import { FlatList, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import ListItem from './components/ListItem'
import UserApi from '../controller/APIs/UserAPIs'

const Home = () => {
    const [data, setData] = useState([])

    const getData = () =>{
        UserApi.getUser()
        .then(data => {
            setData(data)
        })
        .catch(error => console.log(error))
    }

    useEffect(() => {
        getData()
    })
    return (
        <View>
            <FlatList
                data={data}
                keyExtractor={data.id}
                renderItem={(item) => <ListItem item={item.item} />}
            />
        </View>
    )
}

export default Home

const styles = StyleSheet.create({})
