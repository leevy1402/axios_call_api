import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native'
import Constants from '../../controller/Constants'

const ListItem = ({ item }) => {
    const navigation = useNavigation()
    return (
        <TouchableOpacity
            style={styles.item}
            onPress={() => {
                navigation.navigate(Constants.screens.detail, { data: item.id })
            }}
        >
            <View style={styles.row}>
                <Image source={{ uri: item.avatar }} style={styles.avatar} />
                <Text style={styles.name}>{item.first_name + ' ' + item.last_name}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default ListItem

const styles = StyleSheet.create({
    avatar: {
        width: 45,
        height: 45,
        borderRadius: 45,
        margin: 10
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    name: {
        fontSize: 20,
        fontWeight: 'bold'
    }
})
