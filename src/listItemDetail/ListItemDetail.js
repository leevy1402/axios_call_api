import { Image, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import UserApi from '../controller/APIs/UserAPIs'
import { useRoute } from '@react-navigation/native'
import Constants from '../controller/Constants'

const ListItemDetail = () => {
    const route = useRoute()
    const [data, setData] = useState({})
    useEffect(() => {
        UserApi.getDetail(route.params?.data)
            .then((response) => setData(response))
            .catch((error) => console.log(error))
    })
    return (
        <View style={styles.container}>
            <View style={styles.card}>
                <Image source={{ uri: data.avatar }} style={styles.avatar} />
                <Text style={styles.name}>{data.last_name + ' ' + data.first_name}</Text>
                <Text style={styles.email}>{data.email}</Text>
            </View>
        </View>
    )
}

export default ListItemDetail

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    card: {
        width: Constants.dimensions.window.width * 0.8,
        height: 250,
        backgroundColor: '#f9f9f9',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        padding: 10,
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: 80,
        height: 80,
        borderRadius: 40,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,

        elevation: 2,
        marginBottom: 15
    },
    name: {
        fontSize: 22,
        fontWeight: 'bold',
        letterSpacing: 3,
        textTransform: 'uppercase'
    }
})
